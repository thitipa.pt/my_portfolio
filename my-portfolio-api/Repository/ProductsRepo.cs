﻿using my_portfolio_api.DataClass;
using my_portfolio_api.Interface;
using my_portfolio_api.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace my_portfolio_api.Repository
{
    public class ProductsRepo : IProducts
    {
        private readonly my_portfolioContext _context;

        public ProductsRepo(my_portfolioContext context)
        {
            _context = context;
        }


        public IEnumerable<ProductResponse> GetAllProducts()
        {
            return _context.Products.Select(x=>new ProductResponse 
            {
                Name = x.Name,
                Description = x.Description,
                Price = x.Price,
                Stock = x.Stock
            }).ToList();
        }


        public ProductResponse GetAllProductsById(int id)
        {
            return _context.Products.Where(x=>x.Id == id).Select(x => new ProductResponse
            {
                Name = x.Name,
                Description = x.Description,
                Price = x.Price,
                Stock = x.Stock
            }).FirstOrDefault();
        }


        public object CreatProduct(Products param)
        {
            try
            {
                _context.Products.Add(param);
                _context.SaveChanges();
                return new { Status = "ok" };
            }
            catch(Exception e)
            {
                return new { Status = "Error:" + e.Message };
            }
            
        }

        public object UpdateProduct(Products param, int id)
        {
            try
            {
                var temp = _context.Products.Find(id);
                if (temp != null)
                {
                    temp.Name = param.Name;
                    temp.Description = param.Description;
                    temp.Price = param.Price;
                    temp.Stock = param.Stock;
                    _context.Products.Update(temp);
                    _context.SaveChanges();
                    return new { Status = "ok" };
                }
                else
                {
                    return new { Status = "Product not found." };
                }
                 
            }
            catch (Exception e)
            {
                return new { Status = "Error:" + e.Message };
            }

        }
        
        public object increaseStock(Products param, int id)
        {
            try
            {
                var temp = _context.Products.Find(id);
                if (temp != null)
                {
                    temp.Stock = temp.Stock + 1;
                    _context.Products.Update(temp);
                    _context.SaveChanges();
                    return new { Status = "ok" };
                }
                else
                {
                    return new { Status = "Product not found." };
                }

            }
            catch (Exception e)
            {
                return new { Status = "Error:" + e.Message };
            }

        }


        public object decreaseStock(Products param, int id)
        {
            try
            {
                var temp = _context.Products.Find(id);
                if (temp != null)
                {
                    temp.Stock = temp.Stock - 1;
                    _context.Products.Update(temp);
                    _context.SaveChanges();
                    return new { Status = "ok" };
                }
                else
                {
                    return new { Status = "Product not found." };
                }

            }
            catch (Exception e)
            {
                return new { Status = "Error:" + e.Message };
            }

        }

        public object DeleteProduct(int id)
        {
            try
            {
                var temp = _context.Products.Find(id);
                if (temp != null)
                {
                    _context.Products.Remove(temp);
                    _context.SaveChanges();
                    return new { Status = "ok" };
                }
                else
                {
                    return new { Status = "Id not found." };
                }


            }
            catch (Exception e)
            {
                return new { Status = "Error:" + e.Message };
            }
        }

        public IEnumerable<ProductResponse> SearchProduct(SearchProduct search)
        {
            return _context.Products.Where(x => x.Name.Contains(search.Search) || x.Description.Contains(search.Search))
                   .Select(x => new ProductResponse
            {
                Name = x.Name,
                Description = x.Description,
                Price = x.Price,
                Stock = x.Stock
            }).ToList();
        }

        





    }
}
