﻿using my_portfolio_api.Interface;
using my_portfolio_api.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace my_portfolio_api.Repository
{
    public class AuthRepo : IAuth
    {
        private readonly my_portfolioContext _context;

        public AuthRepo(my_portfolioContext context)
        {
            _context = context;
        }

        public User Login(string username, string password)
        {
            var user = _context.User.FirstOrDefault(x => x.Username == username && x.Password == password);
            return user;
        }

    }
}
