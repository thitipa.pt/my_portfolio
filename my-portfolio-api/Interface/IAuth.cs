﻿using my_portfolio_api.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace my_portfolio_api.Interface
{
    public interface IAuth
    {
        User Login(string username, string password);
    }
}
