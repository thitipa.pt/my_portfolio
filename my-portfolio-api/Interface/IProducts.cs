﻿using my_portfolio_api.DataClass;
using my_portfolio_api.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace my_portfolio_api.Interface
{
    public interface IProducts
    {
        IEnumerable<ProductResponse> GetAllProducts();
        ProductResponse GetAllProductsById(int id);
        object CreatProduct(Products param);
        object UpdateProduct(Products param, int id);
        object DeleteProduct(int id);
        object increaseStock(Products param, int id);
        object decreaseStock(Products param, int id);
        IEnumerable<ProductResponse> SearchProduct(SearchProduct Search);








    }
}
