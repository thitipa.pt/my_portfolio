﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using my_portfolio_api.DataClass;
using my_portfolio_api.Interface;
using my_portfolio_api.Model;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace my_portfolio_api.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class ProductsController : ControllerBase
    {
        private readonly IProducts _repo;

        public ProductsController(IProducts repo) {
            _repo = repo;
        }

        [HttpGet]
        public IActionResult GetAllProducts()
        {
            return Ok(_repo.GetAllProducts());
        }


        [HttpGet("{id}")]
        public IActionResult GetAllProductsById(int id)
        {
            return Ok(_repo.GetAllProductsById(id));
        }

        [HttpPost]
        public IActionResult CreatProduct(Products param)
        {
            return Ok(_repo.CreatProduct(param));
        }

        [HttpPut("{id}")]
        public IActionResult UpdateProduct(Products param, int id)
        {
            return Ok(_repo.UpdateProduct(param,id));
        }

        [HttpPut("increaseStock/{id}")]
        public IActionResult increaseStock(Products param, int id)
        {
            return Ok(_repo.increaseStock(param, id));
        }


        [HttpPut("decreaseStock/{id}")]
        public IActionResult decreaseStock(Products param, int id)
        {
            return Ok(_repo.decreaseStock(param, id));
        }

        [HttpDelete("{id}")]
        public IActionResult DeleteProduct(int id)
        {
            return Ok(_repo.DeleteProduct(id));
        }

        [HttpPost("SearchProduct")]
        public IActionResult SearchProduct(SearchProduct Search)
        {
            return Ok(_repo.SearchProduct(Search));
        }


    }


}

